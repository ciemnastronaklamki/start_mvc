start_mvc.sh

Script automate some things. I wrote it to automatically mount my php project directories in /srv/http. After that httpd and mysqld daemons are started. So don't need to mess in Apache config (too much). Script is designed for Arch Linux with systemd as daemons manager, but it should run in similar environment as well.
You can use it.
