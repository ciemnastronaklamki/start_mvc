# !/bin/bash
# Author: Michał Misztal (ciemna.strona.klamki@gmail.com)

show_help() {
    echo ""
    echo ""
    echo "sh start_mvc.sh -m FOLDER"
    echo "  Bind FOLDER as /srv/http and start httpd and mysqld services. FOLDER is directory name relative to script"
    echo "  Root password needed."
    echo ""
    echo "sh start_mvc.sh -u"
    echo "  Unmount directory /srv/http and stop httpd and mysqld services."
    echo ""
}

show_error() {
    echo ""
    echo ""
    echo "Wrong usage of script. Run with --help parameter to show help."
    echo ""
    echo ""
}

show_error_folder() {
    echo ""
    echo ""
    echo "Directory not exist"
    echo ""
    echo ""
}

show_error_httpd_is_mounted() {
    echo ""
    echo ""
    echo "/srv/http is mounted. Run with -u option to unmount."
    echo ""
    echo ""
}

if [ $1 = "-m" ]; then

    if [ -n $2 ];then


        if mount | grep /srv/http 2>/dev/null;then

            show_error_httpd_is_mounted
            
        else
        
            currentDir=$(pwd)
            fullPath="$currentDir/$2"
            
            if [ -d "$fullPath" ];then
                su -c "mount -o bind $fullPath /srv/http && systemctl start httpd"
            else
                show_error_folder
            fi
            
        fi
        
    fi
    
elif [ $1 = "-u" 2>/dev/null ]; then
    su -c "systemctl stop httpd && umount /srv/http"
elif [ $1 = "--help" 2>/dev/null ];then
    show_help
else
    show_error
fi
